:author: The KiCad Team
:doctype: book
:toc:
:ascii-ids:

= KiCad

_Reference manual_

[[copyright]]
*Copyright*

This document is Copyright (C) 2010-2021 by its contributors as listed below. You may distribute it
and/or modify it under the terms of either the GNU General Public License
(http://www.gnu.org/licenses/gpl.html), version 3 or later, or the Creative Commons Attribution
License (http://creativecommons.org/licenses/by/3.0/), version 3.0 or later.

All trademarks within this guide belong to their legitimate owners.

[[contributors]]
*Contributors*

Jean-Pierre Charras, Fabrizio Tappero, Jon Evans.

[[feedback]]
*Feedback*

The KiCad project welcomes feedback, bug reports, and suggestions related to the software or its
documentation.  For more information on how to sumbit feedback or report an issue, please see the
instructions at https://www.kicad.org/help/report-an-issue/

== Introduction

KiCad is an open-source software suite for creating electronic circuit schematics and printed
circuit boards (PCBs).  KiCad supports an integrated design workflow in which a schematic and
corresponding PCB are designed together, as well as standalone workflows for special uses.  KiCad
also includes several utilities to help with circuit and PCB design, including a PCB calculator for
determining electrical properties of circuit structures, a Gerber viewer for inspecting 
manufacturing files, and an integrated SPICE simulator for inspecting circuit behavior.

KiCad runs on all major operating systems and a wide range of computer hardware.  It supports PCBs
with up to 32 copper layers and is suitable for creating designs of all complexities.  KiCad is
developed by a volunteer team of software and electrical engineers around the world with a mission
of creating free and open-source electronics design software suitable for professional designers.

The latest version of this documentation is available at https://docs.kicad.org. 

=== System Requirements

KiCad is capable of running on a wide variety of hardware and operating systems, but some tasks
may be slower or more difficult on lower-end hardware.  For the best experience, a dedicated
graphics card and display with 1920x1080 or higher resolution is recommended.

Please check the KiCad website for the latest system requirements:
https://kicad.org/help/system-requirements/

=== KiCad files and folders

KiCad creates and uses files with the following specific file extensions (and folders)
for schematic and board editing.

*Project manager file:*
[width="100%",cols="20%,80%",]
|=================================================================
|*.kicad_pro |Project file, containing settings that are shared between the schematic and PCB
|*.pro |Legacy (KiCad 5.x and earlier) project file.
Can be read and will be converted to a _.kicad_pro_ file by the project manager.
|=================================================================

*Schematic editor files:*
[width="100%",cols="20%,80%",]
|=================================================================
|*.kicad_sch |Schematic files containing all info and the components themselves.
|*.kicad_sym |Schematic symbol library file, containing the component descriptions: graphic shape, pins, fields.
|*.sch |Legacy (KiCad 5.x and earlier ) schematic file.
Can be read and will be converted to a _.kicad_sch_ file on write.
|*.lib |Legacy (KiCad 5.x and earlier ) schematic library file. Can be read but not written.
|*.dcm |Legacy (KiCad 5.x and earlier ) schematic library documentation. Can be read but not written.
|*-cache.lib |Legacy (KiCad 5.x and earlier ) schematic component library cache file.
Required for proper loading of a legacy schematic (_.sch_) file.
|sym-lib-table |Symbol library list (_symbol library table_):
list of symbol libraries available in the schematic editor.
|=================================================================

*Board editor files and folders:*
[width="100%",cols="20%,80%",]
|=================================================================
|*.kicad_pcb |Board file containing all info but the page layout.
|*.pretty |Footprint library folders. The folder itself is the library.
|*.kicad_mod |Footprint files, containing one footprint description each.
|*.kicad_dru |Design rules file, containing custom design rules for a certain _.kicad_pcb_ file.
|*.brd |Legacy (KiCad 4.x and earlier ) board file.
Can be read, but not written, by the current board editor.
|*.mod |Legacy (KiCad 4.x and earlier ) footprint library file.
Can be read by the footprint or the board editor, but not written.
|fp-lib-table |Footprint library list (_footprint library table_):
list of footprint libraries available in the board editor.
|fp-info-cache |Cache to speed up loading of footprint libraries.
|=================================================================

*Common files:*
[width="100%",cols="20%,80%",]
|=================================================================
|*.kicad_wks |Page layout (drawing border and title block) description file
|*.net |Netlist file created by the schematic, and read by the board editor.
This file is associated to the .cmp file, for users who prefer a separate file
for the component/footprint association.
|*.kicad_prl |Local settings for the current project, helps Kicad remember
the last used settings such as layer visibility or selection filter.
May not need to be distributed with the project or put under version control.
|=================================================================

*Other files:*
[width="100%",cols="20%,80%",]
|=================================================================
|*.cmp |Association between components used in the schematic and their footprints.
It can be created by Pcbnew and imported by Eeschema.
Its purpose is to import changes from Pcbnew to Eeschema, for users
who change footprints inside Pcbnew (for instance using _Exchange Footprints_ command)
and want to import these changes in schematic.
|=================================================================

*Other files:*

They are generated by KiCad for fabrication or documentation.
[width="100%",cols="20%,80%",]
|=================================================================
|*.gbr |Gerber files, for fabrication.
|*.drl |Drill files (Excellon format), for fabrication.
|*.pos |Position files (ASCII format), for automatic insertion machines.
|*.rpt |Report files (ASCII format), for documentation.
|*.ps |Plot files (Postscript), for documentation.
|*.pdf |Plot files (PDF format), for documentation.
|*.svg |Plot files (SVG format), for documentation.
|*.dxf |Plot files (DXF format), for documentation.
|*.plt |Plot files (HPGL format), for documentation.
|=================================================================

*Storing and and sending KiCad files*

KiCad schematic and board files contain all the schematic symbols and footprints used in the
design, so you can back up or send these files by themselves with no issue.  Some important
design information is stored in the project file (_.kicad_pro_), so if you are sending a complete
design, make sure to include it.

Some files, such as the project local settings file (_.kicad_prl_) and the _fp-info-cache_ file,
are not necessary to send with your project.  If you use a version control system such as Git to
keep track of your KiCad projects, you may want to add these files to the list of ignored files so
that they are not tracked.


== Installing and Upgrading KiCad

[[settings]]
=== Importing settings

Each major release of KiCad has its own configuration, so that you may run multiple KiCad versions
on the same computer without the configurations interfering.  The first time you run a new version
of KiCad, you will be asked how to initialize the settings:

image::images/configure_settings.png[]

If a previous version of KiCad is detected, you will have the option to import the settings from
that version.  The location of the previous configuration files is detected automatically, but you
may override it to choose another location if desired.

By default, the schematic symbol and footprint library tables from the previous version of KiCad
will also be imported.  If you would like to start with no library configuration, uncheck the
**Import library configuration from previous version** checkbox.

You may also choose to start with default settings if you do not want to import settings from a
previous version.

KiCad stores the settings files in a folder inside your user directory.  Each KiCad version will
store its settings in a subfolder of that folder (except for KiCad 5.1 and earlier, which did not
use subfolders).  Those folders are:

[width="75%",cols="25%,75%"]
|===========================
|Windows |`%APPDATA%\kicad`
|Linux   |`~/.config/kicad`
|Mac OS  |`/Users/<username>/Library/Preferences/kicad`
|===========================

=== Migrating files from previous versions

Modern versions of KiCad can open files created in earlier versions, but can only write files in
the latest formats.  This means that in general, there are no special steps to migrate files from
a previous version besides opening the files.  In some cases, the file extension for a file has
changed from one KiCad version to the next.  After opening these files, they will be saved in the
new format with the new file extension.  The old files will not be deleted automatically.

In general, files created or modified by one version of KiCad **cannot** be opened by older
versions of KiCad.  For this reason, it is important to keep backup copies of your projects when
testing a new KiCad release, until you are confident that you will not need to use the older
KiCad version anymore.

NOTE: Hotkey configurations are not imported from previous versions at this time.  You can manually
      import hotkey configurations by copying the various `*.hotkeys` files from the old version
      configuration directory to the new one.  If you do so, please note that KiCad will not
      automatically detect conflicts such as one key being assigned to multiple actions.


== Using KiCad project manager

KiCad project manager (kicad or kicad.exe) is a tool which can easily run the other tools
(schematic and board editors, Gerber viewer and utility tools) when creating a design.

Running the other tools from KiCad manager has some advantages:

* cross probing between schematic editor and board editor.

* synchronization of the design between the schematic editor and board editor (without creating
netlist files)

KiCad currently only supports having one project open at a time.  When running the schematic and
board editors from the KiCad project manager, you can only edit the schematics and board associated
with the open project.  When these tools are run in _stand alone_ mode, you can open any file in
any project, but cross probing between tools can give strange results.

=== Project manager window

image::images/main_window.png[scaledwidth="90%"]

The KiCad project manager window is composed of a tree view on the left showing the files
associated with the open project, and a launcher on the right containing shortcuts to the various
editors and tools.

=== Project tree view

The tree view shows a list of files inside the project folder.  Double-clicking on a file in the
tree view will open it in the associated editor.  Right-clicking on a file will open a context
menu with some file manipulation commands.

image::images/project_tree.png[scaledwidth="35%"]

NOTE: Only files that KiCad understands how to open are displayed in the project tree view.

=== Side toolbar

The toolbar on the left side of the window provides shortcuts for common project operations:

[width="100%",cols="10%,90%",]
|=======================================================================
|image:images/icons/new_project_24.png[]
|Create a new project.
|image:images/icons/open_project_24.png[]
|Open an existing project.
|image:images/icons/zip_24.png[]
|Create a zip archive of the whole project. This includes schematic
files, libraries, PCB, etc.
|image:images/icons/unzip_24.png[]
|Extract a project zip archive into a directory. Files in the destination
directory will be overwritten.
|image:images/icons/reload_24.png[]
|Refresh the tree view, to detect changes made on the filesystem.
|image:images/icons/directory_browser_24.png[]
|Open the project working directory in a file explorer.
|=======================================================================

=== Creating a new project

Most KiCad designs start with the creation of a project.  There are two ways to create a project
from the KiCad project manager: you may create an empty project, or create a project based on an
existing template.  This section will cover the creation of a new, empty project.  Creating
projects from templates is covered in the <<project-templates,Project Templates>> section.

To create a new project, use the **New Project...** command in the **File** menu, click the
**New Project** button in the top toolbar, or use the keyboard shortcut (`Ctrl+N` by default).

You will be prompted for a name to give your project.  By default, a directory will be created for
your project with the same name.  For example, if you enter the name `MyProject`, KiCad will create
the directory `MyProject` and the project file `MyProject/MyProject.kicad_pro` inside it.

If you already have a directory to store your project files in, you can uncheck the
_Create a new directory for the project_ checkbox in the **New Project** dialog.

NOTE: It is strongly recommended that you store each KiCad project inside its own directory.

Once you select the name of your project, KiCad will create the following files inside the project
directory:

[width="100%",cols="27%,73%",]
|=================================================================
|example.kicad_pro |KiCad project file.
|example.kicad_sch |Main schematic file.
|example.kicad_pcb |Printed circuit board file.
|=================================================================

=== Importing a project from another EDA tool

KiCad is able to import files created by some other software packages. Currently the following
types of project are supported:

[width="100%",cols="27%,73%",]
|=============================================
|*.sch, *.brd |Eagle 6.x or newer (XML format)
|*.csa, *.cpa |CADSTAR archive format
|=============================================

To import a project from one of these tools, choose the appropriate option from the **Import
Non-KiCad Project** submenu of the **File** menu.

You will be prompted to select either a schematic or a board file in the import file browser
dialog. The imported schematic and board files should have the same base file name (e.g.
project.sch and project.brd). Once the requested files are selected, you will be asked to select a
directory to store the resulting KiCad project.

[[kicad-manager-configuration]]
== KiCad configuration

The KiCad preferences can always be accessed from the **Preferences** menu, or by using the hotkey
(default `Ctrl+,`). The Preferences dialog is shared between the running KiCad tools.  Some
preferences apply to all tools, and some are specific to a certain tool (such as the schematic or
board editor).

=== Common preferences

image::images/preferences_common.png[width="100%"]

**Accelerated graphics antialising:** KiCad can use different methods to prevent aliasing
(jagged lines) when rendering using a graphics card.  Different methods may look better on
different hardware, so you may want to experiment to find the one that looks best to you.

**Fallback graphics antialiasing:** KiCad can also apply antialiasing when using the fallback
graphics mode.  Enabling this feature may result in poor performance on some hardware.

**Text editor:** Choose a text editor to use when opening text files from the project tree view.

**PDF viewer:** Choose a program to use when opening PDF files.

**Show icons in menus:** Enables icons in drop-down menus throughout the KiCad user interface.

NOTE: Icons in menus are not displayed on some operating systems.

**Icon theme:** Sets whether to use the icon theme designed for light window backgrounds or dark
window backgrounds.  The default setting of Automatic will choose the theme based on the lightness
of the operating system window theme.

**Icon scale:** Sets the size of the icons used in menus and buttons throughout KiCad.  Choose
_Automatic_ to pick an appropriate icon scale automatically based on your operating system
settings.

**Canvas scale:** Sets the scale of the drawing canvas used in the KiCad editors.  Choose
_Automatic_ to pick an appropriate canvas scale automatically based on your operating system
settings.

**Apply icon scaling to fonts:** This setting will scale fonts used in the UI according to the icon
scale setting.  This is not needed for most users, but may improve the look of KiCad on certain
Linux platforms when using a high-DPI display.

**Warp mouse to origin of moved object:** When enabled, the mouse cursor will be repositioned
(warped) to the origin of an object when you start a move command on that object.

**First hotkey selects tool:** When disabled, pressing the hotkey for a command such as _Add Wire_
will immediately start the command at the current cursor location.  When enabled, pressing the
hotkey the first time will just select the _Add Wire_ tool but will not immediately begin a wire.

**Remember open files for next project launch:** When enabled, KiCad will automatically re-open any
files that were previously open when a project is re-opened.

**Auto save:** When editing schematics and board files, KiCad can automatically save your work
periodically.  Set to `0` to disable this feature.

**File history size:** Configure the number of entries in the list of recently-opened files

**3D cache file duration:** KiCad creates a cache of 3D models in order to speed up the 3D viewer.
You can configure how long to keep this cache before deleting old files.

**Automatically backup projects:** When enabled, KiCad projects will be archived to ZIP files
automatically according to the settings below.  The archives will be stored in a subfolder of the
project folder.  Backups are created when saving files in the project.

**Create backups when auto save occurs:** When enabled, a backup will be created every time an
automatic file save occurs (if the backup is permitted by the settings below).  This setting has
no effect if the auto save interval is set to `0` (disabled).

**Maximum backups to keep:** When creating a new backup, the oldest backup file will be deleted to
keep the total number of backup files below this limit.

**Maximum backups per day:** When creating a new backup, the oldest backup file created on the
current day will be deleted to stay below this limit.

**Minimum time between backups:** If backup is triggered (for example, by saving a board file),
the backup will not be created if an existing backup file is newer than this limit.

**Maximum total backup size:** When creating a new backup file, the oldest backup files will be
deleted to keep the total size of the backup files directory below this limit.

**Remember open files for next project launch:** When checked, KiCad will re-open the schematic and
board editor if they were open the last time you closed the project manager.

=== Mouse and touchpad preferences

image::images/preferences_mouse_and_touchpad.png[width="100%"]

**Center and warp cursor on zoom:** When enabled, zooming using the hotkeys or mouse wheel will
cause the view to be centered on the cursor location.

**Use zoom acceleration:** When enabled, scrolling the mouse wheel or touchpad faster will cause
the zoom to change faster.

**Zoom speed:** Controls how much the zoom changes for a given amount of scrolling the mouse wheel
or touchpad.  Use _Automatic_ to set a default value depending on your operating system.

**Automatically pan while moving object:** When enabled, the view can be panned while moving an
object by moving close to the edge of the canvas.

**Auto pan speed:** Controls how fast the canvas pans while moving an object.

**Mouse buttons:** You can set the behavior of dragging the middle and right mouse buttons to zoom
the view, pan the view, or have no effect.  You can also set the behavior of dragging the left
mouse button depending on whether or not any objects are already selected in the editing canvas.

NOTE: The left mouse button is always used for selecting and manipulating objects.

**Mouse wheel and touchpad scrolling:** You can set the behavior of scrolling the mouse wheel or
vertical motion of the touchpad while pressing certain modifier keys.

**Pan left/right with horizontal movement:** When enabled, you can pan the view using the touchpad
or horizontal scroll wheel (if present on your mouse).

=== Hotkey preferences

image::images/preferences_hotkeys.png[width="100%"]

You can use this dialog to customize the hotkeys used to control KiCad.  The hotkeys in the
_Common_ section are shared between every KiCad program. Hotkeys for each specific KiCad program
are shown when that program is running.  You can assign the same hotkey to a different action in
different KiCad programs (for example, the schematic editor and the board editor), but you cannot
assign a hotkey to more than one action in the same program.

There are many available commands, and so not all of them have a hotkey assigned by default.  You
can add a hotkey to any command by double-clicking on the command in the list.  If you choose a
hotkey that is already assigned to a different command, you can choose to use that hotkey on your
chosen command, which will remove the hotkey assignment from the conflicting command.

Changes that you have made to hotkey assignments are shown with a `*` character at the end of the
command name.  You can undo changes to a specific command by right-clicking that command and
selecting _Undo Changes_, or you can undo all changes with the button below the command list.

**Importing hotkeys**

Hotkey preferences are stored in `.hotkeys` files in the KiCad settings directory (see the
<<settings,Settings>> section for information about where the settings directory is on your
operating system).  If you have configured KiCad hotkeys the way you like on one computer, you can
transfer that configuration to another computer by importing the appropriate `.hotkeys` file(s).

=== Paths configuration

In KiCad, one can define paths using an __environment variable__.
A few environment variables are internally defined by KiCad,
and can be used to define paths for libraries, 3D shapes, etc.

This is useful when absolute paths are not known or are subject to change (e.g.
when you transfer a project to a different computer), and also when one base
path is shared by many similar items. Consider the following which may be
installed in varying locations:

* Schematic symbol libraries
* Footprint libraries
* 3D shape files used in footprint definitions

For instance, the path to the *_connect.pretty_* footprint library,
when using the *KISYSMOD* environment variable, would be defined as
*_$\{KISYSMOD\}/connect.pretty_*

The *Preferences* -> *Configure Paths...* menu
allows you to define paths for some built-in KiCad environment variables,
and add your own environment variables to define personal paths, if needed.

*KiCad environment variables:*
[width="100%",cols="30%,70%",]
|=================================================================
|KICAD6_SYMBOL_DIR |Base path of symbol library files.
|KICAD6_FOOTPRINT_DIR |Base path of footprint library files.
|KICAD6_3DMODEL_DIR |Base path of 3D models used in footprints.
|KICAD6_TEMPLATE_DIR |Location of project templates installed with KiCad.
|KICAD_USER_TEMPLATE_DIR |Location of personal project templates.
|KICAD6_SCRIPTING_DIR |Location of Python scripts installed with KiCad.
|KICAD6_USER_SCRIPTING_DIR |Location of personal Python scripts.
|=================================================================

image::images/configure_path_dlg.png[scaledwidth="80%"]

NOTE: You cannot override an environment variable that has been set outside of KiCad by using the
      Configure Paths dialog.  Any variable that has been set externally will be shown as read-only
      in the dialog.

Some advanced environment variables can be set to customize KiCad's behavior. These variables are
not shown in the environment variable configuration dialog by default.  Changing these variables
will not result in KiCad moving any files from the default location to the new location, so if you
change these variables you will need to copy any desired settings or files manually.

*Additional environment variables:*
[width="100%",cols="30%,70%",]
|=================================================================
|KICAD_CONFIG_HOME |Base path of KiCad configuration files. Subdirectories will be created
    within this directory for each KiCad minor version.
|KICAD_DOCUMENTS_HOME |Base path of KiCad user-modifiable documents, such as templates,
    Python scripts, libraries, etc. Subdirectories will be created within this directory for each
    KiCad minor version.
|=================================================================

Note also the environment variable *KIPRJMOD* is *always* internally
defined by KiCad, and is the **current project absolute path**.

For instance, *_$\{KIPRJMOD\}/connect.pretty_* is always the *_connect.pretty_*
folder (the pretty footprint library) found inside **the current project folder**.

*If you modify the configuration of paths, please quit and restart KiCad
to avoid any issues in path handling.*

[[libraries-configuration]]
=== Libraries configuration

The *Preferences* -> *Manage Symbol Libraries...* menu let you manage the
library list files called **symbol library table** (sym-lib-table).

Likewise, use the *Preferences* -> *Manage Footprint Libraries...* menu to manage
the library list files called **footprint library table** (fp-lib-table).

There are 2 library list files:
the first (located in the user home directory) is global for all projects and
the second (located in the project directory) is optional and specific to the project.


[[project-templates]]
== Project templates

Using a project template facilitates setting up a new project with predefined
settings. Templates may contain pre-defined board outlines, connector
positions, schematic elements, design rules, etc. Complete schematics and/or
PCBs used as seed files for the new project may even be included.

=== Using templates

The *_File -> New Project -> New Project from Template_* menu will
open the Project Template Selector dialog:

image::images/template_selector.png[scaledwidth="80%"]

A single click on a template's icon will display the template information,
and a further click on the OK button creates the new project. The template
files will be copied to the new project location and renamed to reflect
the new project's name.

After selection of a template:

image::images/template_selected.png[scaledwidth="80%"]

[[template-locations]]
=== Template Locations:

KiCad looks for template files in the following paths:

- path defined in the environment variable KICAD_USER_TEMPLATE_DIR

- path defined in the environment variable KICAD_TEMPLATE_DIR

- System templates:
  <kicad bin dir>/../share/kicad/template/

- User templates:

  ** Unix:
     ~/kicad/template/

  ** Windows:
     C:\Documents and Settings\username\My Documents\kicad\template
     or C:\Users\username\Documents\kicad\template

  ** Mac:
     ~/Documents/kicad/template/

=== Creating templates

The template name is the directory name where the template
files are stored. The metadata directory is a subdirectory
named *meta* containing files describing the template.

The metadata consists of one required file, and may contain optional files.
All files must be created by the user using a text editor or previous
KiCad project files, and placed into the required directory structure.

All files and directories in a template are copied to the new project
path when a project is created using a template, except *meta*.
Files and directories containing the template name will be renamed
with the new project file name.

For example, creating a project called *newproject* from a template
named *example*:

[width="100%",cols="50%,50%",]
|=======================================================================
|Files in template *example* directory
|Files created in project *newproject* directory
|
example.kicad_pro +
example.kicad_sch +
example.kicad_pcb +
example-first.kicad_sch +
second-example.kicad_sch +
third.kicad_sch +
third.kicad_pcb
|
*newproject.kicad_pro* +
*newproject.kicad_sch* +
*newproject.kicad_pcb* +
*newproject-first.kicad_sch* +
*second-newproject.kicad_sch* +
third.kicad_sch +
third.kicad_pcb
|=======================================================================

A template does not need to contain a complete project, if a required
project file is missing, KiCad will create it using its default create
project behavior:

[width="100%",cols="50%,50%",]
|=======================================================================
|Files in template *example* directory
|Files created in *newproject* directory
|
example.kicad_sch +
first-example.kicad_sch +
first-example.kicad_pcb +
second-example.kicad_sch +
second-example.kicad_pcb
|
*newproject.kicad_sch* +
*first-newproject.kicad_sch* +
*first-newproject.kicad_pcb* +
*second-newproject.kicad_sch* +
*second-newproject.kicad_pcb* +
*newproject.kicad_pro* (default) +
*newproject.kicad_pcb* (default)
|=======================================================================

As an exception to the template name renaming rule, if one project file
(.kicad_pro) exists and its name doesn't match the template name,
KiCad will do the renaming based on that project file name instead:

[width="100%",cols="50%,50%",]
|=======================================================================
|Files in template *example* directory
|Files created in *newproject* directory
|
example.kicad_sch +
example.kicad_pcb +
*first-example.kicad_pro* +
first-example.kicad_sch +
first-example.kicad_pcb +
second-example.kicad_sch +
second-example.kicad_pcb
|
example.kicad_sch +
example.kicad_pcb +
*newproject.kicad_pro* +
*newproject.kicad_sch* +
*newproject.kicad_pcb* +
second-example.kicad_sch +
second-example.kicad_pcb
|=======================================================================

NOTE: It is not recommended to create a template with multiple project files.

==== Template example

Here is an example showing project files for *raspberrypi-gpio* template:

image::images/template_tree.png[scaledwidth="70%"]

And the metadata files:

image::images/template_tree_meta.png[scaledwidth="70%"]

==== Required File:

[width="100%",cols="20%,80%",]
|=================================================================
|meta/info.html |HTML-formatted information describing the template.
|=================================================================

The <title> tag determines the actual name of the template that is exposed
to the user for template selection. Note that the project template name
will be cut off if it's too long.

Using HTML means that images can be easily in-lined without having to
invent a new scheme. Only basic HTML tags can be used in this document.

Here is a sample *info.html* file:

[source,html]
----------------------------------------------------------------
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html;
charset=windows-1252">
<TITLE>Raspberry Pi - Expansion Board</TITLE>
</HEAD>
<BODY LANG="fr-FR" DIR="LTR">
<P>This project template is the basis of an expansion board for the
<A HREF="http://www.raspberrypi.org/" TARGET="blank">Raspberry Pi $25
ARM board.</A> <BR><BR>This base project includes a PCB edge defined
as the same size as the Raspberry-Pi PCB with the connectors placed
correctly to align the two boards. All IO present on the Raspberry-Pi
board is connected to the project through the 0.1&quot; expansion
headers. <BR><BR>The board outline looks like the following:
</P>
<P><IMG SRC="brd.png" NAME="brd" ALIGN=BOTTOM WIDTH=680 HEIGHT=378
BORDER=0><BR><BR><BR><BR>
</P>
<P>(c)2012 Brian Sidebotham<BR>(c)2012 KiCad Developers</P>
</BODY>
</HTML>
----------------------------------------------------------------

==== Optional Files:

[width="100%",cols="20%,80%",]
|=================================================================
|meta/icon.png |A 64 x 64 pixel PNG icon file which is used as a
clickable icon in the template selection dialog.
|=================================================================

Any other image files used by *meta/info.html*, such as the image of the
board file in the dialog above, are placed in this folder as well.

== Plugin and Content Manager

NOTE: TODO: Write this section
