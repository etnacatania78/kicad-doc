
== Generating outputs

KiCad can generate and export files in a number of different formats useful for manufacturing PCBs
and interfacing with external software.  This functionality is available in the File menu in a few
different sections.  The Fabrication Outputs section contains the most common operations needed to
prepare a PCB for fabrication.  The Export section contains tools for generating files that can be
read by external software.  The Plot function allows you to export 2D line drawings of the PCB in
various formats.  The Print function allows you to send a view of the PCB to a 2D printer.

=== Fabrication outputs and plotting

KiCad uses Gerber files as its primary plotting format for PCB manufacturing.  To create Gerber
files, open the Plot dialog from the File menu, or select Gerbers from the Fabrication Outputs
section of the File menu.  The Plot dialog will open, allowing you to configure and generate
Gerber files.

image::images/plot_dialog.png[scaledwidth="70%"]

==== Plotting options

**Include Layers:** Check that every layer used on your board is enabled in the list.  Disabled
layers will not be plotted.

**Output directory:** Specify the location to save plotted files.  If this is a relative path, it
is created relative to the project directory.

**Plot border and title block:** If enabled, the drawing sheet border and title block will be
plotted on each layer.  This should usually be disabled when plotting Gerber files.

**Plot footprint values:** If enabled, the Value field of each footprint will be plotted on
whatever layer it exists on (unless the field visibility is disabled for a specific footprint).

**Plot reference designators:** If enabled, the Reference Designator field of each footprint will
be plotted on whatever layer it exists on (unless the field visibility is disabled for a specific
footprint).

**Force plotting of invisible values / refs:** If enabled, all footprint values and reference
designators will be plotted, even if the field visibility is disabled for some of these fields.

**Plot Edge.Cuts on all layers:** If enabled, the Edge.Cuts (board outline) layer will be added to
all other layers.  Check with your manufacturer to see what the correct value of this setting is
for their manufacturing process.

**Sketch pads on fabrication layers:** If enabled, footprint pads on fabrication (F.Fab, B.Fab)
layers will be drawn as unfilled outlines rather than filled shapes.

**Do not tent vias:** If enabled, vias will be left uncovered on the solder mask layers (F.Mask,
B.Mask).  If disabled, vias will be covered by solder mask (tented).

NOTE: KiCad does not support tenting or uncovering specific vias.  Tenting may only be controlled
      globally (for all vias on a board).

**Drill marks:** For plot formats other than Gerber, marks may be plotted at the location of all
drilled holes.  Drill marks may be created at the actual size (diameter) of the finished hole, or
at a smaller size.

**Scaling:** For plot formats that support scaling other than 1:1, the plot scale may be set.  The
Auto scaling setting will scale the plot to fit the specified page size.

**Plot mode:** For some plot formats, filled shapes may be plotted as outlines only (sketch mode).

**Use drill/place file origin:** When enabled, the coordinate origin for plotted files will be the
drill/place file origin set in the board editor.  When disabled, the coordinate origin will be the
absolute origin (top left corner of the worksheet).

**Mirrored plot:** For some plot formats, the output may be mirrored horizontally when this option
is set.

**Negative plot:** For some plot formats, the output may be set to negative mode.  In this mode,
shapes will be drawn for the empty space inside the board outline, and empty space will be left
where objects are present in the PCB.

**Check zone fills before plotting:** When enabled, zone fills will be checked (and refilled if
outdated) before generating outputs.  Plot outputs may be incorrect if this option is disabled!

==== Gerber options

**Use Protel filename extensions:** When enabled, the plotted Gerber files will be named with file
extensions based on Protel (`.GBL`, `.GTL`, etc).  When disabled, the files will have the `.gbr`
extension.

**Generate Gerber job file:** When enabled, a Gerber job file (`.gbrjob`) will be generated along
with any Gerber files.  The Gerber job file is an extension to the Gerber format that includes
information about the PCB stackup, materials, and finish.  More information about Gerber job files
is available at link:https://www.ucamco.com/en/gerber/gerber-job-file[the Ucamco website].

**Coordinate format:** Configure how coordinates will be stored in the plotted Gerber files.  Check
with your manufacturer for their recommended setting for this option.

**Use extended X2 format:** When enabled, the plotted Gerber files will use the X2 format, which
includes information about the netlist and other extended attributes.  This format may not be
compatible with older CAM software used by some manufacturers.

**Include netlist attributes:** When enabled, the plotted Gerber files will include netlist
information that can be used for checking the design in CAM software.  When X2 format mode is
disabled, this information is included as comments in the Gerber files.

**Disable aperture macros:** When enabled, all shapes will be plotted as primitives rather than by
using aperture macros.  This setting should only be used for compatibility with old or buggy CAM
software when requested by your manufacturer.

==== PostScript options

**Scale factor:** Controls how coordinates in the board file will be scaled to coordinates in the
PostScript file.  Using a different value for X and Y scale factors will result in a stretched / 
distorted output.  These factors may be used to correct for scaling in the PostScript output device
to achive an exact-scale output.

**Track width correction:** A global factor that is added (or subtracted, if negative) from the
size of tracks, vias, and pads when plotting a PostScript file.  This factor may be uesd to correct
for errors in the PostScript output device to achieve an exact-scale output.

**Force A4 output:** When enabled, the generated PostScript file will be A4 size even if the KiCad
board file is a different size.

==== SVG options

**Units:** Controls the units that will be used in the SVG file.  Since the SVG format has
no specified units system, you must export using the same units setting that you want to use for
importing into other software.

**Precision:** Controls how many significant digits will be used to store coordinates.

==== DXF options

**Plot graphic items using their contours:** Graphic shapes in DXF files have no width.  This
option controls how graphic shapes with a width (thickness) in a KiCad board are plotted to a DXF
file.  When this option is enabled, the outer contour of the shape will be plotted.  When this
option is disabled, the centerline of the shape will be plotted (and the shape's thickness will not
be visible in the resulting DXF file).

**Use KiCad font to plot text:** When enabled, text in the KiCad design will be plotted as graphic
shapes using the KiCad font.  When disabled, text will be plotted as DXF text objects, which will
use a different font and will not appear in exactly the same position and size as shown in the
KiCad board editor.

**Export units:** Controls the units that will be used in the DXF file.  Since the DXF format has
no specified units system, you must export using the same units setting that you want to use for
importing into other software.

==== HPGL options

**Default pen size:** Controls the plotter pen size used to create graphics.

=== Drill files

KiCad can generate CNC drilling files required by most PCB manufacturing processes in either
Excellon or Gerber X2 format.  KiCad can also generate a drill map: a graphical plot of the board
showing drill locations.  Select the Generate Drill Files option from the Fabrication Outputs menu
to open the dialog:

image::images/generate_drill_files_dialog.png[scaledwidth="70%"]

**Output folder:** Choose the folder to save generated drill and map files to.  If a relative path
is entered, it will be relative to the project directory.

**Drill file format:** Choose whether to generate Excellon drill files (required by most PCB
manufacturers) or Gerber X2 files.

**Mirror Y axis:** For Excellon files, choose whether or not to mirror the Y-axis coordinate.  This
option should in general not be used when having PCBs manufactured by a third party, and is
provided for convenience for users who are making PCBs themselves.

**Minimal header:** For Excellon files, choose whether to output a minimal header rather than a
full file header.  This option should not be enabled unless requested by your manufacturer.

**PTH and NPTH in single file:** By default, plated holes and non-plated holes will be generated in
two different Excellon files.  With this option enabled, both will be merged into a single file.
This option should not be enabled unless requested by your manufacturer.

**Oval holes drill mode:** Controls how oval holes are represented in an Excellon drill file.  The
default setting, **Use route command**, is correct for most manufacturers.  Only choose the **Use
alternate drill mode** setting if requested by your manufacturer.

**Map file format:** Choose the output format for plotting a drill map.

**Drill origin:** Choose the coordinate origin for drill files.  **Absolute** will use the page 
origin at the top left corner.  **Drill/place file origin** will use the origin specified in the
board design.

**Drill units:** Choose the units for drill coordinates and sizes.

**Zeros format:** Controls how numbers are formatted in an Excellon drill file.  Select an option
here based on your manufacturer's recommendations.

=== Component placement files

Component placement files are text files that list each component (footprint) on the board along
with its center position and orientation.  These files are usually used for programming
pick-and-place machines, and may be required by your manufacturer if you are ordering
fully-assembled PCBs.

NOTE: A footprint will not appear in generated placement files if the "Exclude from position files"
      option is enabled for that footprint.  This may be used for excluding certain footprints that
      do not represent physical components to be assembled.

image::images/generate_placement_files_dialog.png[scaledwidth="70%"]

**Format:** Choose between generating a plain text (ASCII), comma-separated text (CSV), or Gerber
placement file format.

**Units:** Choose the units for component locations in the placement file.

**Files:** Choose whether to generate separate files for footprints on the front and back of the
board or a single file combining both sides.

**Include only SMD footprints:** When enabled, only footprints with the SMD fabrication attribute
will be included.  Check with your manfuacturer to determine if non-SMD footprints should be
included or excluded from the position file.

**Exclude all footprints with through hole pads:** When enabled, footprints will be excluded from
the placement file if they contain any through-hole pads, even if their fabrication type is set to
SMD.

**Include board edge layer:** For Gerber placement files, controls whether or not the board outline
is included with the footprint placement data.

**Use drill/place file origin:** When enabled, component positions will be relative to the 
drill/place file origin set in the board design.  When disabled, the positions will be relative to
the page origin (upper left corner).

=== Additional fabrication outputs

KiCad can also generate footprint report files, IPC-D-356 netlist files, and a bill of materials
(BOM) from the board design.  These output formats have no configurable options.

=== Printing

KiCad can print the board view to a standard printer using the Print action in the File menu.

image::images/print_dialog.png[scaledwidth="70%"]

**Included layers:** Select the layers to include in the printout.  Unselected layers will be
invisible.

**Output mode:** Choose whether to print in black and white or full color.

**Print border and title block:** When enabled, the page border and title block will be printed.

**Print according to objects tab of appearance manager:** When enabled, any objects that have been
hidden in the Objects tab of the Appearance panel will be hidden in the printout.  When disabled,
these objects will be printed if the layer they appear on is selected in the Included Layers area.

**Print background color:** When printing in full color, this option controls whether or not the
view background color will be printed.

**Use a different color theme for printing:** When printing in full color, this option allows a
different color theme to be used for printing.  When disabled, the color theme used by the board
editor will be used for printing.

**Drill marks:** Controls whether to show drilled holes at their actual size, at a small size, or
hide them from the printout.

**Print mirrored:** When enabled, the printout will be mirrored horizontally.

**Print one page per layer:** When enabled, each layer selected in the Included Layers area will be
printed to an individual page.  If this option is enabled, the **Print board edges on all pages**
option controls whether to add the Edge.Cuts layer to each printed page.

**Scale:** controls the scale of the printout relative to the page size configured in Page Setup.

=== Exporting files

KiCad can export a board design to various third-party formats for use with external software.
These functions are found in the Export section of the File menu.

**Specctra .DSN:** creates a file suitable for importing into certain third-party autorouter
software.  This exporter has no configurable options.

NOTE: TODO: Document GenCAD exporter

NOTE: TODO: Document VRML exporter

NOTE: TODO: Bring IDF exporter docs in to here

NOTE: TODO: Document STEP exporter

NOTE: TODO: Document SVG exporter

NOTE: TODO: Document CMP file exporter

**Hyperlynx:** creates a file suitable for importing into Mentor Graphics (Siemens) HyperLynx
simulation and analysis software.
